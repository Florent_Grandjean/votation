import java.util.ArrayList;
import java.util.List;

public abstract class Sujet {
    protected List<Observateur> observateurs;

    public Sujet() {
        observateurs = new ArrayList<Observateur>();
    }

    public void enregistreObservateur(Observateur electeur) {
        observateurs.add(electeur);
    }

    protected void notifieObservateurs(
            String attributModifie, Object nouvelleValeur) {
        for (int i = 0; i < observateurs.size(); i++) {
            observateurs.get(i).metsAJour(attributModifie, nouvelleValeur);
        }
    }
}
