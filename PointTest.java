

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.Random;

/**
 * Classe-test PointTest.
 *
 * @author  (votre nom)
 * @version (un numéro de version ou une date)
 *
 * Les classes-test sont documentées ici :
 * http://junit.sourceforge.net/javadoc/junit/framework/TestCase.html
 * et sont basées sur le document Š 2002 Robert A. Ballance intitulé
 * "JUnit: Unit Testing Framework".
 *
 * Les objets Test (et TestSuite) sont associés aux classes à tester
 * par la simple relation yyyTest (e.g. qu'un Test de la classe Name.java
 * se nommera NameTest.java); les deux se retrouvent dans le męme paquetage.
 * Les "engagements" (anglais : "fixture") forment un ensemble de conditions
 * qui sont vraies pour chaque méthode Test à exécuter.  Il peut y avoir
 * plus d'une méthode Test dans une classe Test; leur ensemble forme un
 * objet TestSuite.
 * BlueJ découvrira automatiquement (par introspection) les méthodes
 * Test de votre classe Test et générera la TestSuite conséquente.
 * Chaque appel d'une méthode Test sera précédé d'un appel de setUp(),
 * qui réalise les engagements, et suivi d'un appel à tearDown(), qui les
 * détruit.
 */
public class PointTest
{
    private Point point1;
    private ObservateurCoordonnee observat1;
    private ObservateurCoordonnee observat2;
    private ObservateurCoordonnee observat3;
    protected Random rnd = new Random();

    // Définissez ici les variables d'instance nécessaires à vos engagements;
    // Vous pouvez également les saisir automatiquement du présentoir
    // à l'aide du menu contextuel "Présentoir --> Engagements".
    // Notez cependant que ce dernier ne peut saisir les objets primitifs
    // du présentoir (les objets sans constructeur, comme int, float, etc.).
    
    

    /**
     * Constructeur de la classe-test PointTest
     */
    public PointTest()
    {
    }

    /**
     * Met en place les engagements.
     *
     * Méthode appelée avant chaque appel de méthode de test.
     */
    @Before
    public void setUp() // throws java.lang.Exception
    {
        point1 = new Point(5, 12);
        observat1 = new ObservateurCoordonnee("obs1");
        observat2 = new ObservateurCoordonnee("obs2");
        observat3 = new ObservateurCoordonnee("obs3");
        point1.enregistreObservateur(observat1);
        point1.enregistreObservateur(observat2);
        point1.enregistreObservateur(observat3);
    }

    /**
     * Supprime les engagements
     *
     * Méthode appelée après chaque appel de méthode de test.
     */
    @After
    public void tearDown() // throws java.lang.Exception
    {
        //Libérez ici les ressources engagées par setUp()
    }

    @Test
    public void testPointXModification()
    {
        Observateur[] observateurs = {observat1, observat2, observat3};
        String attribut = "x";
        int value = rnd.nextInt(100);
        String modif = String.format("Attribut : %s Valeur : %d", attribut, value);
        point1.setX(value);
        for (Observateur obs : observateurs) {
            assertEquals(modif, obs.getModification());
        }
    }
    
    @Test
    public void testPointYModification()
    {
        Observateur[] observateurs = {observat1, observat2, observat3};
        String attribut = "y";
        int value = rnd.nextInt(100);
        String modif = String.format("Attribut : %s Valeur : %d", attribut, value);
        point1.setY(value);
        for (Observateur obs : observateurs) {
            assertEquals(modif, obs.getModification());
        }
    }
    
    @Test
    public void testPointXYModification()
    {
        Observateur[] observateurs = {observat1, observat2, observat3};
        
        String attribut = "x";
        int value = rnd.nextInt(100);
        String modif = String.format("Attribut : %s Valeur : %d", attribut, value);
        point1.setX(value);
        for (Observateur obs : observateurs) {
            assertEquals(modif, obs.getModification());
        }
        
        attribut = "y";
        value = rnd.nextInt(100);
        modif = String.format("Attribut : %s Valeur : %d", attribut, value);
        point1.setY(value);
        for (Observateur obs : observateurs) {
            assertEquals(modif, obs.getModification());
        }
    }
}

